import tensorflow as tf
import nibabel as nib
import os
from scipy.ndimage import zoom
import numpy as np
#import pandas as pd
folds = 10
factor =1100/folds 
count = 0
'''
for i in range(1113):
    slice_fact = i*factor
    #print(slice_fact)
    print((i)*factor)
    print((i+1)*factor)
    dirs = os.listdir( "brain_nii" )[int((i)*factor):int((i+1)*factor)]
    print(len(dirs))
'''
dirs = os.listdir( "brain_nii" )
nii_list = []

for file in dirs:
    nii_list = []
    print(count)
    #load
    img = nib.load('brain_nii/'+file)
    data = img.get_data()
    #crop
    cropped = data[28:228,28:228,28:228]
    nii_list.append([cropped])
    count += 1
    a= np.array(nii_list,dtype=np.float32)
    print(a.shape)
    out = tf.transpose(a, [0, 2, 3, 4, 1])

    #saving preprocess data (testing)
    np.save('subjects2/'+file[:-4]+'.npy',out)


