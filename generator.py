import nibabel
import tensorflow as tf
import sys
from tensorflow.python.client import device_lib
import numpy as np
import datetime
from tensorflow.keras.utils import to_categorical
import os
import pandas as pd
from sklearn.model_selection import KFold
 


local_device_protos = device_lib.list_local_devices()
print(local_device_protos)

# Tensorflow working version: 1.5.1
print("Tensorflow working version: %s" % tf.__version__)

print("Python version:", sys.version)


batch_size = 4 # first test with 7

#checkpoint_callback = tf.keras.callbacks.ModelCheckpoint("cross_wtf.h5", monitor='val_accuracy', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', save_freq='epoch')
earlystop_callback = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', patience=15)
adam = tf.keras.optimizers.Adam(learning_rate=0.000001,beta_1=0.9,beta_2 = 0.999,amsgrad= False)


dirs = os.listdir( "brain_nii" )
subjects = pd.read_csv('subjects.csv')
columns = subjects[['Subject','class']]
labels= columns.set_index('Subject').T.to_dict('list')
#print(labels)
Y = []
for j in range(len(dirs)):
    id,ext = dirs[j].split('.')
    try:
        Y.append(labels[int(id)][0])
    except:
        Y.append(0)

print(np.array(Y).shape)
Y = np.array(Y)
Y = np.where(Y==-1, 0, Y)


def generate_arrays(dirs,labels):
    while 1:
        counter = 0
        _X = np.empty([1,200,200,200, 1])
        _Y = []
        for file in dirs:
            #print(file)
            img = np.load('subjects2/'+file.split('.')[0]+'.npy')
            #print(a.shape)
            _Y.append(labels[int(file.split('.')[0])])
#            np.concatenate((a, b), axis=0)
            _X = np.concatenate((_X,img) , axis=0)
            #print("fuckkkkkkkk",Y)


            counter = counter+1
            if counter%batch_size == 0:
                #X = tf.transpose(_X, [0, 2, 3,4 ,1])
                _Y = np.array(_Y)
                _Y = np.where(_Y==-1, 0, _Y)
                _Y = to_categorical(_Y, num_classes=2)
                x, y = _X[1:], _Y
                #print(y)
                #print(x.shape)
                _X = np.empty([1,200,200,200, 1])
                _Y = []
                yield x, y



#model.fit_generator(generate_arrays(dirs,Y), steps_per_epoch = int(1108/5), epochs= 10, callbacks=[tensorboard_callback, checkpoint_callback, earlystop_callback], verbose=1)
#model.fit(X, Y, epochs=500, batch_size=5, callbacks=[tensorboard_callback, checkpoint_callback, earlystop_callback],shuffle = True)



strategy = tf.distribute.OneDeviceStrategy(device="/gpu:0")
hist_vacc = []
hist_acc = []
#with strategy.scope():
with tf.device("/device:GPU:0"):#XLA_ 
    n_split=4
    counter = 0
    for train_index,test_index in KFold(n_split).split(dirs):
        model = tf.keras.models.Sequential([
	#tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv3D(8, (3,3,3), activation='relu', padding='SAME', input_shape= (200,200,200,1), name="conv1"),
            tf.keras.layers.Conv3D(8, (3,3,3), activation='relu', padding='SAME', name="conv2"),
            tf.keras.layers.MaxPooling3D((2, 2, 2)),
            tf.keras.layers.Conv3D(16, (3,3,3), activation='relu', padding='SAME', name="conv3"),
            tf.keras.layers.Conv3D(16, (3,3,3), activation='relu', padding='SAME', name="conv4"),
            tf.keras.layers.MaxPooling3D((2, 2, 2)),
            tf.keras.layers.Conv3D(32, (3,3,3), activation='relu', padding='SAME', name="conv5"),
            tf.keras.layers.Conv3D(32, (3,3,3), activation='relu', padding='SAME', name="conv6"),
            tf.keras.layers.Conv3D(32, (3,3,3), activation='relu', padding='SAME', name="conv7"),
            tf.keras.layers.MaxPooling3D((2, 2, 2)),
            tf.keras.layers.Conv3D(64, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.Conv3D(64, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.Conv3D(64, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.MaxPooling3D((2, 2, 2)),
            tf.keras.layers.Conv3D(128, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.Conv3D(128, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.Conv3D(128, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.MaxPooling3D((2, 2, 2)),
            tf.keras.layers.Conv3D(256, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.Conv3D(256, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.Conv3D(256, (3,3,3), activation='relu', padding='SAME'),
            tf.keras.layers.MaxPooling3D((2, 2, 2)),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(256, activation='relu'),
            tf.keras.layers.Dropout(rate= 0.3),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dense(128, activation='relu'),
            tf.keras.layers.Dropout(rate= 0.3),
            tf.keras.layers.Dense(2, activation='sigmoid', name='predictions')
        ])
        
        log_dir="cross_wtf/fit/fold"+str(counter)#+"made on"+ datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1, profile_batch = 100000000)
        checkpoint_callback = tf.keras.callbacks.ModelCheckpoint("models/cross_wtf"+str(counter)+".h5", monitor='val_accuracy', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', save_freq='epoch')
        model.compile(optimizer=adam, loss='categorical_crossentropy',metrics=['accuracy'])
        #print(train_index)
        #print(test_index)
        #model = tf.keras.models.load_model('wtf.h5')
        print(model.summary())
        for ilayer, layer in enumerate(model.layers):
            print("{:3.0f} {:10}".format(ilayer, layer.name))
        X_t = [dirs[i] for i in train_index]
        X_v = [dirs[i] for i in test_index]

        h = model.fit_generator(generate_arrays(X_t,labels), validation_data=generate_arrays(X_v,labels), validation_steps= len(X_v)//batch_size ,steps_per_epoch = len(X_t)//batch_size, epochs= 100, callbacks=[tensorboard_callback, checkpoint_callback, earlystop_callback], verbose=1)
        hist_vacc.append(h.history['val_accuracy'])
        hist_acc.append(h.history['accuracy'])
        model.save('models/korolev_generator_model'+str(counter)+'.h5')
        counter = counter +1

#http://cs231n.github.io/neural-networks-3/#sanitycheck

#https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/

#print(histories)
l1 = []
l2 = []
print("------------------")
for i in range(len(hist_vacc)):
    l1.append(hist_vacc[i][-1])
    l2.append(hist_acc[i][-1])
    print(hist_acc[i][-1], " : ", hist_vacc[i][-1])
print('------------------')
avg1 = sum(l1)/len(hist_acc)  
avg2 = sum(l2)/len(hist_acc)  
print(avg2," : " ,avg1)
tf.keras.backend.clear_session()


#model.fit_generator(generate_arrays(dirs,Y), steps_per_epoch = int(1108/5), epochs= 10, callbacks=[tensorboard_callback, checkpoint_callback, earlystop_callback], verbose=1)
#model.fit(X, Y, epochs=500, batch_size=5, callbacks=[tensorboard_callback, checkpoint_callback, earlystop_callback],shuffle = True)

'''
    model = tf.keras.models.Sequential([
	#tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Conv3D(8, (3,3,3), activation='relu', padding='SAME', input_shape= (155,165,195,1), name="conv1"),
	tf.keras.layers.Conv3D(8, (3,3,3), activation='relu', padding='SAME', input_shape= (155,165,195,1), name="conv2"),
        tf.keras.layers.MaxPooling3D((2, 2, 2)),
        tf.keras.layers.Conv3D(16, (3,3,3), activation='relu', padding='SAME', name="conv3"),
        tf.keras.layers.Conv3D(16, (3,3,3), activation='relu', padding='SAME', name="conv4"),
        tf.keras.layers.MaxPooling3D((2, 2, 2)),
        tf.keras.layers.Conv3D(32, (3,3,3), activation='relu', padding='SAME', name="conv5"),
        tf.keras.layers.Conv3D(32, (3,3,3), activation='relu', padding='SAME', name="conv6"),
        tf.keras.layers.Conv3D(32, (3,3,3), activation='relu', padding='SAME', name="conv7"),
        tf.keras.layers.MaxPooling3D((2, 2, 2)),
        tf.keras.layers.Conv3D(64, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.Conv3D(64, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.Conv3D(64, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.MaxPooling3D((2, 2, 2)),
        tf.keras.layers.Conv3D(128, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.Conv3D(128, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.Conv3D(128, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.MaxPooling3D((2, 2, 2)),
	tf.keras.layers.Conv3D(256, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.Conv3D(256, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.Conv3D(256, (3,3,3), activation='relu', padding='SAME'),
        tf.keras.layers.MaxPooling3D((2, 2, 2)),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(256, activation='relu'),
	tf.keras.layers.Dropout(rate= 0.3),
	tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dropout(rate= 0.3),
        tf.keras.layers.Dense(2, activation='sigmoid', name='predictions')
    ])
'''

